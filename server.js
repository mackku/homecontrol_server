var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var path = require('path');
var port = process.env.PORT || 7777;
var io = require('socket.io')(http);
var sensorLib = require('node-dht-sensor');
var gpio = require('rpi-gpio');
var seconds;
var relay_state = [-1, -1, -1, -1];
var calibrateTime = 10;

var r_pin = [3, 5, 11, 13, 40, 38, 36, 37, 35, 33, 31 ,29];

app.get('/', function(req, res) {
    res.render('index2');
});

app.get('/test', function(req, res) {
    res.render('test3');
});

io.on('connection', function(socket) {

  socket.emit('message', 'ยินดีด้วย คุณได้ทำการเชื่อมต่อกับเซิร์ฟเวอร์แล้ว');
  socket.on('message', function(data){
    console.log(data);
  });
 
  //Living Room
	socket.on('LVR_Relay', function(data) {
      	//console.log(data);
      	
     //Relay 1
      	if(data.number == 1) {
      		var state_1 = data.state;
      		if(state_1 == 1) {
					   r_control.on();
					    relay_state[0] = 1;
	        } else if(state_1 == 0){
						r_control.off();	
						relay_state[0] = 0;
			}      	
		}	 
	//Relay 2
       	if(data.number == 2 ) {
      		var state_2 = data.state;
      		if(state_2 == 1) {
					r_control2.on();
					relay_state[1] = 1;
      		} else if(state_2 == 0){
					r_control2.off();
					relay_state[1] = 0;
			} 
		}	
	//Relay 3
       	if(data.number == 3) {
      		var state_3 = data.state;
      		if(state_3 == 1) {
					r_control3.on();
					relay_state[2] = 1;
      		} else if(state_3 == 0){
					r_control3.off();
					relay_state[2] = 0;
			} 
		}		
	//Relay 4
       	if(data.number == 4) {
      		var state_4 = data.state;
      		if(state_4 == 1) {
					r_control4.on();
					relay_state[3] = 1;
      		} else if(state_4 == 0){
					r_control4.off();
					relay_state[3] = 0;
			} 
		}	
      	io.emit('LVR_Relay', data);
    });

	  //When socket is disconnected
	  socket.on('disconnect', function(){
		console.log('มีผู้ใช้ได้ยกเลิกการเชื่อมต่อ');
	  });

});



app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade'); 
app.use(express.static('public'));


http.listen(port, function(){
  console.log('ระบบเซิร์ฟเวอร์บ้านอัจฉริยะได้เริ่มการทำงานแล้ว คุณสามารถใช้พอร์ต ' + port + " เพื่อเชื่อมต่อ");
});


var r_control = {
	on: function(){
		gpio.setup(r_pin[0], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[0], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 1 is on');
                                });
                        }	
	},
	off: function(){
                gpio.setup(r_pin[0], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[0], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 1 is off');
                                });
                                }

        }
}
var r_control2 = {
        on: function(){
                gpio.setup(r_pin[1], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[1], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 2 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[1], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[1], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 2 is off');
                                });
                                }

        }
}
var r_control3 = {
        on: function(){
                gpio.setup(r_pin[2], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[2], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 3 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[2], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[2], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 3 is off');
                                });
                                }

        }
}
var r_control4 = {
        on: function(){
                gpio.setup(r_pin[3], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[3], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 4 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[3], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[3], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 4 is off');
                                });
                                }

        }
}
var r_control5 = {
        on: function(){
                gpio.setup(r_pin[4], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[4], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 5 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[4], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[4], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 5 is off');
                                });
                                }

        }
}
var r_control6 = {
        on: function(){
                gpio.setup(r_pin[5], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[5], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 6 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[5], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[5], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 6 is off');
                                });
                                }

        }
}
var r_control7 = {
        on: function(){
                gpio.setup(r_pin[6], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[6], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 7 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[6], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[6], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 7 is off');
                                });
                                }

        }
}
var r_control8 = {
        on: function(){
                gpio.setup(r_pin[7], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[7], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 8 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[7], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[7], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 8 is off');
                                });
                                }

        }
}
var r_control9 = {
        on: function(){
                gpio.setup(r_pin[8], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[8], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 9 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[8], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[8], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 9 is off');
                                });
                                }

        }
}
var r_control10 = {
        on: function(){
                gpio.setup(r_pin[9], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[9], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 10 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[9], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[9], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 10 is off');
                                });
                                }

        }
}
var r_control11 = {
        on: function(){
                gpio.setup(r_pin[10], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[10], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 11 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[10], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[10], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 11 is off');
                                });
                                }

        }
}
var r_control12 = {
        on: function(){
                gpio.setup(r_pin[11], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[11], false, function(err) {
                                if (err) throw err;
                                console.log('Relay 12 is on');
                                });
                        }
        },
        off: function(){
                gpio.setup(r_pin[11], gpio.DIR_OUT, write);
                        function write() {
                                gpio.write(r_pin[11], true, function(err) {
                                if (err) throw err;
                                console.log('Relay 12 is off');
                                });
                                }

        }
}
//

var status = {
	check: function() {
			gpio.on('change', function(channel,value) {
				console.log('Ćhannel ' + channel + ' value is now ' + value);
			});
	}
}
gpio.setup(3, gpio.DIR_IN, gpio.EDGE_BOTH);

//Temperature sensor

var sensor = {
          initialize: function () {
              return sensorLib.initialize(22, 4);
          },
          read: function () {
          var readout = sensorLib.read();
			
       //Emit temperature and humidity    
	   io.emit('LVR_Temp', {
       		 temp : readout.temperature.toFixed(2),
        	 humidity : readout.humidity.toFixed(2)
      	   });
      	   
       //Read every 2 seconds
          setTimeout(function () {
                sensor.read();
            }, 2000);
          }
}

//PIR
var PIR = {
	read: function () {
		gpio.setup(15, gpio.DIR_IN, readInput);
		
		function readInput() {
			gpio.read(15, function(err, value) {
				console.log(value);
				if(value == true) {
					io.emit('LVR_PIR', {
						 pir : 1
					});
					io.emit('LVR_PIR', {
						 pir : 0
					});
				} else {
					io.emit('LVR_PIR', {
						 pir : 0
					});
				}
			});
		}
		
          setTimeout(function () {
                PIR.read();
            }, 1000);
    }
}

if (sensor.initialize()) {
          sensor.read();
   } else {
          console.warn('Failed to initialize sensor');
}






