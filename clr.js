var gpio = require('rpi-gpio');

var r_pin = [3, 5, 11, 13, 40, 38, 36, 37, 35, 33, 31 ,29];
gpio.setup(r_pin[0], gpio.DIR_OUT, pause);
gpio.setup(r_pin[1], gpio.DIR_OUT, pause); 
gpio.setup(r_pin[2], gpio.DIR_OUT, pause);
gpio.setup(r_pin[3], gpio.DIR_OUT, pause);
gpio.setup(r_pin[4], gpio.DIR_OUT, pause);
gpio.setup(r_pin[5], gpio.DIR_OUT, pause);
gpio.setup(r_pin[6], gpio.DIR_OUT, pause);
gpio.setup(r_pin[7], gpio.DIR_OUT, pause);
gpio.setup(r_pin[8], gpio.DIR_OUT, pause);
gpio.setup(r_pin[9], gpio.DIR_OUT, pause);
gpio.setup(r_pin[10], gpio.DIR_OUT, pause);
gpio.setup(r_pin[11], gpio.DIR_OUT, pause);


function pause() {
    setTimeout(closePins, 1000);
}
 
function closePins() {
    gpio.destroy(function() {
        console.log('All pins unexported');
    });
}
